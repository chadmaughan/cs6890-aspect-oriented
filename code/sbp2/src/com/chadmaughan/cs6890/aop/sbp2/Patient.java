package com.chadmaughan.cs6890.aop.sbp2;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.chadmaughan.cs6890.aop.sbp2.annotation.Decrypt;
import com.chadmaughan.cs6890.aop.sbp2.annotation.Encrypted;
import com.chadmaughan.cs6890.aop.sbp2.annotation.Protected;

@SuppressWarnings("serial")
public class Patient extends Person {

	public enum Gender {
		MALE, FEMALE;

		@Override 
		public String toString() {
			return super.toString();
		}
	}

	int id;
	String gender;
	
	Date birthdate;
	
	Set<Physician> physicians;	
	Set<EmergencyContact> emergencyContacts;

	List<Allergy> allergies;
	List<Diagnosis> diagnoses;
	List<HealthIssue> healthIssues;
	List<Prescription> prescriptions;	
	List<Surgery> surgeries;	
	
	public Patient() {}
	
	public Patient(Name primaryName) {
		super(primaryName);
	}
	
	public List<Allergy> getAllergies() {
		return allergies;
	}

	public void setAllergies(List<Allergy> allergies) {
		this.allergies = allergies;
	}

	@Protected(value="birthdate")
	public Date getBirthdate() {
		return birthdate;
	}

	@Protected(value="birthdate")
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public List<Diagnosis> getDiagnoses() {
		return diagnoses;
	}

	public void setDiagnoses(List<Diagnosis> diagnoses) {
		this.diagnoses = diagnoses;
	}

	public Set<EmergencyContact> getEmergencyContacts() {
		return emergencyContacts;
	}

	public void setEmergencyContacts(Set<EmergencyContact> emergencyContacts) {
		this.emergencyContacts = emergencyContacts;
	}

	@Decrypt
	public String getGender() {
		return gender;
	}

	public void setGender(@Encrypted String gender) {
		this.gender = gender;
	}

	public List<HealthIssue> getHealthIssues() {
		return healthIssues;
	}

	public void setHealthIssues(List<HealthIssue> healthIssues) {
		this.healthIssues = healthIssues;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Set<Physician> getPhysicians() {
		return physicians;
	}

	public void setPhysicians(Set<Physician> physicians) {
		this.physicians = physicians;
	}

	public List<Prescription> getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(List<Prescription> prescriptions) {
		this.prescriptions = prescriptions;
	}

	public boolean addAllergy(Allergy allergy) {
		if(allergies == null)
			allergies = new ArrayList<Allergy>();
		return allergies.add(allergy);
	}
	
	public boolean addDiagnosis(Diagnosis diagnosis) {
		if(diagnoses == null)
			diagnoses = new ArrayList<Diagnosis>();
		return diagnoses.add(diagnosis);
	}

	public boolean addEmergencyContact(EmergencyContact emergencyContact) {
		if(emergencyContacts == null)
			emergencyContacts = new HashSet<EmergencyContact>();
		return emergencyContacts.add(emergencyContact);
	}
	
	public boolean addHealthIssue(HealthIssue healthIssue) {
		if(healthIssues == null)
			healthIssues = new ArrayList<HealthIssue>();
		return healthIssues.add(healthIssue);
	}

	public boolean addPhysician(Physician physician) {
		if(physicians == null)
			physicians = new HashSet<Physician>();
		return physicians.add(physician);
	}

	public boolean addPrescription(Prescription prescription) {
		if(prescriptions == null)
			prescriptions = new ArrayList<Prescription>();
		return prescriptions.add(prescription);
	}

	public boolean addSurgery(Surgery surgery) {
		if(surgeries == null)
			surgeries = new ArrayList<Surgery>();
		return surgeries.add(surgery);
	}

	public List<Surgery> getSurgeries() {
		return surgeries;
	}
	
	public boolean removeAllergy(Allergy allergy) {
		return healthIssues.remove(allergy);
	}
	
	public boolean removeDiagnosis(Diagnosis diagnosis) {
		return diagnoses.remove(diagnosis);
	}

	public boolean removeEmergencyContact(EmergencyContact emergencyContact) {
		return emergencyContacts.remove(emergencyContact);
	}
	
	public boolean removeHealthIssue(HealthIssue healthIssue) {
		return healthIssues.remove(healthIssue);
	}

	public boolean removePhysician(Physician diagnosis) {
		return physicians.remove(diagnosis);
	}

	public boolean removePrescription(Prescription prescription) {
		return prescriptions.remove(prescription);
	}

	public boolean removeSurgery(Surgery surgery) {
		return surgeries.remove(surgery);
	}

	public void setSurgeries(List<Surgery> surgeries) {
		this.surgeries = surgeries;
	}
}
