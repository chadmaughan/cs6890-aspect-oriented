package com.chadmaughan.cs6890.aop.sbp2.session;

import java.util.HashSet;
import java.util.Set;

import com.chadmaughan.cs6890.aop.sbp2.Person;

/**
 * Partial fulfillment of Instructions 2 from sbp2.pdf
 * @author chadmaughan
 *
 */
public enum Session {

    INSTANCE;

    private String userName;
    private Set<String> userPermissions;
    private Set<Person> persons;
    
    public void addSessionPerson(Person person) {
    	if(persons == null)
    		persons = new HashSet<Person>();
    	persons.add(person);
    }

    public void addPermission(String permission) {

    	if(userPermissions == null)
    		userPermissions = new HashSet<String>();

		userPermissions.add(permission);
    }
    
    public boolean canView(String user, String property) {
    	return allowed(user, property);
    }
    
    public boolean canModify(String user, String property) {
    	return allowed(user, property);
    }

    private boolean allowed(String user, String property) {
    	boolean result = false;
    	
    	if(userPermissions != null) {
	    	for(String s : userPermissions) {
	    		if(s.equals(property)) {
	    			result = true;
	    		}
	    	}
    	}
    	return result;
    }
    
    public Set<Person> getPersons() {
		return persons;
	}

	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
