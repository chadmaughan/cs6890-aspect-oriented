package com.chadmaughan.cs6890.aop.sbp2;

import java.util.Date;

@SuppressWarnings("serial")
public class Diagnosis extends Base {

	Date date;
	String condition;
	String notes;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
