package com.chadmaughan.cs6890.aop.sbp2;

@SuppressWarnings("serial")
public class Allergy extends Base {

	int severity;
	String allergen;

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public String getAllergen() {
		return allergen;
	}

	public void setAllergen(String allergen) {
		this.allergen = allergen;
	}
}
