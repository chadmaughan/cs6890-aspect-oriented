package com.chadmaughan.cs6890.aop.sbp2.aspect;

import com.chadmaughan.cs6890.aop.sbp2.annotation.Protected;
import com.chadmaughan.cs6890.aop.sbp2.session.Session;

public aspect Protect {

	pointcut protectGet(Protected protectedField): execution(@Protected * *.get*())
		&& @annotation(protectedField)
		&& !within(Encrypt) 
		&& !within(Protect) 
		&& !within(Serialize);

	pointcut protectSet(Object object, Protected protectedField): execution(@Protected * *.set*(..))
		&& @annotation(protectedField)
		&& args(object)
		&& !within(Encrypt) 
		&& !within(Protect) 
		&& !within(Serialize);

	Object around(Protected protectedField) : protectGet(protectedField) {
		
		String user = Session.INSTANCE.getUserName();
		String property = protectedField.value();
		if(authorized(user, property)) {
			return proceed(protectedField);
		}
		return new Object();
    }

	Object around(Object object, Protected protectedField) : protectSet(object, protectedField) {
		
		String user = Session.INSTANCE.getUserName();
		String property = protectedField.value();
		if(authorized(user, property)) {
			return proceed(object, protectedField);
		}		
		return new Object();
    }

	private boolean authorized(String user, String property) {
		
		System.out.println("Checking user: '" + user + "' can access property: '" + property + "'");

		if(Session.INSTANCE.canView(user, property)) {
			System.out.println("Authorized");
			return true;
		}
		
		System.out.println("Not authorized");
		return false;
	}
}