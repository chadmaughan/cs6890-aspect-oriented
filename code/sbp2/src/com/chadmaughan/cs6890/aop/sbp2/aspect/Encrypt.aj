package com.chadmaughan.cs6890.aop.sbp2.aspect;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.chadmaughan.cs6890.aop.sbp2.annotation.Decrypt;
import com.chadmaughan.cs6890.aop.sbp2.annotation.Encrypted;

public aspect Encrypt {

	// encrypt either methods or variables annotated w/ @Encrypted
	pointcut encrypt(Object o) : call(@Encrypted * *.set*(..)) && args(o) || call(* *.set*(@Encrypted (*))) && args(o);
	pointcut decrypt() : call(@Decrypt * *.get*()) && args();

	DESKeySpec keySpec = null;
	SecretKeyFactory keyFactory = null;
	SecretKey key = null;
	String encrypted = null;
	
	Object around(Object o) : encrypt(o) {

		System.out.println("Encrypting");

		try {
			
			String s = (String) o;
			
			keySpec = new DESKeySpec("Secret".getBytes("UTF8")); 
			keyFactory = SecretKeyFactory.getInstance("DES");
			key = keyFactory.generateSecret(keySpec);
			
			byte[] cleartext = s.getBytes("UTF8");      
	
			Cipher cipher = Cipher.getInstance("DES");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			encrypted = DatatypeConverter.printBase64Binary(cipher.doFinal(cleartext));
	
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		return encrypted;
	}
	
	String around() : decrypt() {

		System.out.println("Decrypting");

		try {

			String s = proceed();

			// DECODE
			byte[] encryptedBytes = DatatypeConverter.parseBase64Binary(s);
	
			Cipher cipher = Cipher.getInstance("DES");
			cipher.init(Cipher.DECRYPT_MODE, key);
	
			byte[] decryptedBytes = (cipher.doFinal(encryptedBytes));
	
			return new String(decryptedBytes);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

}