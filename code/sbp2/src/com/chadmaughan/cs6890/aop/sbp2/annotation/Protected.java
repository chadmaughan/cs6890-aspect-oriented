package com.chadmaughan.cs6890.aop.sbp2.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value=RetentionPolicy.RUNTIME)
public @interface Protected {
    String value();
}