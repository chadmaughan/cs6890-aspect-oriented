package com.chadmaughan.cs6890.aop.sbp2;

import java.util.ArrayList;
import java.util.List;

import com.chadmaughan.cs6890.aop.sbp2.util.Util;

@SuppressWarnings("serial")
public class Person extends Base implements Comparable<Person> {

	Name primaryName;
	List<Name> names;
	List<Address> addresses;
	List<PhoneNumber> phoneNumbers;
	
	public Person() {}
	
	public Person(Name primaryName) {
		this.primaryName = primaryName;
	}
	
	public Name getPrimaryName() {
		return primaryName;
	}

	public void setPrimaryName(Name primaryName) {
		this.primaryName = primaryName;
	}

	public List<Name> getNames() {
		return names;
	}

	public void setNames(List<Name> names) {
		this.names = names;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public boolean addPhoneNumber(PhoneNumber phoneNumber) {
		if(phoneNumbers == null)
			phoneNumbers = new ArrayList<PhoneNumber>();
		return phoneNumbers.add(phoneNumber);
	}
	
	public boolean removePhoneNumber(PhoneNumber phoneNumber) {
		return phoneNumbers.remove(phoneNumber);
	}

	public boolean addAddress(Address address) {
		if(addresses == null)
			addresses = new ArrayList<Address>();
		return addresses.add(address);
	}
	
	public boolean removeAddress(Address address) {
		return addresses.remove(address);
	}

	public boolean addName(Name name) {
		if(names == null)
			names = new ArrayList<Name>();
		return names.add(name);
	}
	
	public boolean removeName(Name name) {
		return names.remove(name);
	}

	public float match(Person person) {
		return Util.computeLevenshteinDistance(person.getPrimaryName().getFormattedName(), this.getPrimaryName().getFormattedName());
	}
	
	@Override
	public int compareTo(Person person) {
		if(person != null) {
			if(this.getPrimaryName() != null && person.getPrimaryName() != null) {
				return this.getPrimaryName().getFormattedName().compareTo(person.getPrimaryName().getFormattedName());
			}
		}
		return 0;
	}
}
