package com.chadmaughan.cs6890.aop.sbp2;

import com.chadmaughan.cs6890.aop.sbp2.util.Util;

@SuppressWarnings("serial")
public class Name extends Base implements Comparable<Name> {

	public enum type {
		BIRTH, MARRIED
	}
	
	String salutation;
	String firstName;
	String middleName;
	String lastName;
	String suffix;

	public Name() {}
	
	public Name(String firstName, String middleName, String lastName) {
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
	}
	
	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getSortName() {
		return lastName + ", " + firstName + " " + middleName;
	}
	
	public String getFormattedName() {
		return salutation + " " + firstName + " " + middleName + " " + lastName + " " + suffix;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append((salutation == null) ? "" : salutation);
		builder.append((firstName == null) ? "" : " " + firstName);
		builder.append((middleName == null) ? "" : " " + middleName);
		builder.append((lastName == null) ? "" : " " + lastName);
		builder.append((suffix == null) ? "" : " " + suffix);
		return builder.toString();
	}

	@Override
	public int compareTo(Name name) {
		return this.getFormattedName().compareTo(name.getFormattedName());
	}

	public float match(Name name) {
		return Util.computeLevenshteinDistance(name.getFormattedName(), this.getFormattedName());
	}
}
