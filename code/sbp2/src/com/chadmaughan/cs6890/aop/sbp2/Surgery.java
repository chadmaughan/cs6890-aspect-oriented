package com.chadmaughan.cs6890.aop.sbp2;

import java.util.Date;

@SuppressWarnings("serial")
public class Surgery extends Base {

	Date date;
	String type;
	String notes;

	public Surgery() {}

	public Surgery(String type, String notes) {
		setNotes(notes);
		setType(type);
		setDate(new Date());
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.notes);
		builder.append(this.type);
		builder.append(this.date);
		return builder.toString();
	}
}
