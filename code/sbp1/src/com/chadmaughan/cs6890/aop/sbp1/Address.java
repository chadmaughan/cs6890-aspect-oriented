package com.chadmaughan.cs6890.aop.sbp1;

import com.chadmaughan.cs6890.aop.sbp1.util.Util;

public class Address implements Comparable<Address> {

	public enum type {
		HOME, WORK
	}

	String streetLine1;
	String streetLine2;
	String city;
	String state;
	String postalCode;

	public String getStreetLine1() {
		return streetLine1;
	}

	public void setStreetLine1(String streetLine1) {
		this.streetLine1 = streetLine1;
	}

	public String getStreetLine2() {
		return streetLine2;
	}

	public void setStreetLine2(String streetLine2) {
		this.streetLine2 = streetLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public int compareTo(Address address) {
		return this.getFormattedAddress().compareTo(address.getFormattedAddress());
	}
	
	public String getFormattedAddress() {
		StringBuilder result = new StringBuilder();
		result.append(streetLine1);
		result.append(System.getProperty("line.separator"));
		result.append(streetLine2);
		result.append(System.getProperty("line.separator"));
		result.append(city);
		result.append(System.getProperty("line.separator"));
		result.append(state);
		result.append(System.getProperty("line.separator"));
		result.append(postalCode);
		return result.toString();
	}
	
	public float match(Address address) {
		return Util.computeLevenshteinDistance(address.getFormattedAddress(), this.getFormattedAddress());
	}
}
