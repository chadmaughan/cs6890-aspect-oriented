package com.chadmaughan.cs6890.aop.sbp1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.chadmaughan.cs6890.aop.sbp1.util.Util;

public class Person {

	Name primaryName;
	List<Name> names;
	List<Address> addresses;
	List<PhoneNumber> phoneNumbers;
	
	public Name getPrimaryName() {
		return primaryName;
	}

	public void setPrimaryName(Name primaryName) {
		this.primaryName = primaryName;
	}

	public List<Name> getNames() {
		return names;
	}

	public void setNames(List<Name> names) {
		this.names = names;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public boolean addPhoneNumber(PhoneNumber phoneNumber) {
		if(phoneNumbers == null)
			phoneNumbers = new ArrayList<PhoneNumber>();
		return phoneNumbers.add(phoneNumber);
	}
	
	public boolean removePhoneNumber(PhoneNumber phoneNumber) {
		return phoneNumbers.remove(phoneNumber);
	}

	public boolean addAddress(Address address) {
		if(addresses == null)
			addresses = new ArrayList<Address>();
		return addresses.add(address);
	}
	
	public boolean removeAddress(Address address) {
		return addresses.remove(address);
	}

	public boolean addName(Name name) {
		if(names == null)
			names = new ArrayList<Name>();
		return names.add(name);
	}
	
	public boolean removeName(Name name) {
		return names.remove(name);
	}

	public float match(Person person) {
		return Util.computeLevenshteinDistance(person.getPrimaryName().getFormattedName(), this.getPrimaryName().getFormattedName());
	}
	
	/**
	 * Satisfies sbp1.pdf Instructions 4a
	 * @return list of sorted names
	 */
	public List<Name> getSortedNames() {
		Collections.sort(this.names, new NameComparator());
		return this.names;
	}
	
	class NameComparator implements Comparator<Name> {

		@Override
		public int compare(Name n1, Name n2) {
			if(n1 != null && n2 != null) {
				if(n1.getSortName() != null && n2.getSortName() != null) {
					return n1.getSortName().compareTo(n2.getSortName());
				}
				else if(n1.getLastName() != null && n2.getLastName() != null) {
					return n1.getLastName().compareTo(n2.getLastName());
				}
			}
			return 0;
		}
	}
}
