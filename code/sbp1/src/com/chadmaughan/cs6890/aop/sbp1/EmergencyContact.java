package com.chadmaughan.cs6890.aop.sbp1;

public class EmergencyContact extends Person {

	String relationship;

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
}
