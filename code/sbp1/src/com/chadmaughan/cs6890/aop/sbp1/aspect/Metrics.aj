package com.chadmaughan.cs6890.aop.sbp1.aspect;

/**
 * Calculates execution times of all methods (except any getters/setters)
 * Satisfies Instructions 5b of sbp1.pdf
 * 
 * @author chadmaughan
 */
public aspect Metrics {

	pointcut calculate() : call(* *.*(..)) && 
							!call(* *.get*(..)) && 
							!call(* *.set*(..)) && 
							!within(Logging) && 
							!within(Metrics);

	Object around() : calculate() {

		long start = System.nanoTime();
        
		Object ret = proceed();
        
		long end = System.nanoTime();
        
		System.out.println(String.format("%10s", (end-start)) + " nanoseconds for: " + thisJoinPointStaticPart.getSignature());
        
		return ret;
	}
	
}
