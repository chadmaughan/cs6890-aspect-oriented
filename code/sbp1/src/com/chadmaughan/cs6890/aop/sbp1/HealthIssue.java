package com.chadmaughan.cs6890.aop.sbp1;

import java.util.Date;

public class HealthIssue {

	Date beganOn;
	Date endedOn;
	String symptomOrObservation;

	public Date getBeganOn() {
		return beganOn;
	}

	public void setBeganOn(Date beganOn) {
		this.beganOn = beganOn;
	}

	public Date getEndedOn() {
		return endedOn;
	}

	public void setEndedOn(Date endedOn) {
		this.endedOn = endedOn;
	}

	public String getSymptomOrObservation() {
		return symptomOrObservation;
	}

	public void setSymptomOrObservation(String symptomOrObservation) {
		this.symptomOrObservation = symptomOrObservation;
	}
}
