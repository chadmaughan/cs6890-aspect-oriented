package com.chadmaughan.cs6890.aop.sbp1.aspect;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Satisfies Instructions 5a of sbp1.pdf
 * @author chadmaughan
 *
 */
public aspect Logging {

	private int depth;	
	
	private BufferedWriter out;
	
	pointcut file() : execution(* Main.main(..));
	pointcut log() : call(* *.*(..)) && !within(Logging) && !within(Metrics);

	before() : log() {
		this.write(thisJoinPoint);
		depth++;
	}
	
	after() : log() {
		depth--;
	}

	private void write(Object message) {

		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < depth; i++) {
			builder.append("    ");
		}
		builder.append(message + System.getProperty("line.separator"));

		try {
			out.write(builder.toString());
		}
		catch (IOException e) {
			e.printStackTrace();
		}		
	}

	/**
	 * Handles the opening of the file before 'Main.main' execution
	 */
	before() : file() {

		// open a file in the tmp dir
		String path = System.getProperty("java.io.tmpdir") + "sbp1.log";
		System.out.println("Opening log file with absolute path: " + path);

		out = null;
		try {
			out = new BufferedWriter(new FileWriter(path, true));
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Closes up the file used for logging after 'Main.main' is done
	 */
	after() : file() {
		System.out.println("Closing log file");

		try {
			out.close();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
