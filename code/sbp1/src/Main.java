import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.chadmaughan.cs6890.aop.sbp1.Address;
import com.chadmaughan.cs6890.aop.sbp1.Allergy;
import com.chadmaughan.cs6890.aop.sbp1.Diagnosis;
import com.chadmaughan.cs6890.aop.sbp1.EmergencyContact;
import com.chadmaughan.cs6890.aop.sbp1.HealthIssue;
import com.chadmaughan.cs6890.aop.sbp1.Name;
import com.chadmaughan.cs6890.aop.sbp1.Patient;
import com.chadmaughan.cs6890.aop.sbp1.PhoneNumber;
import com.chadmaughan.cs6890.aop.sbp1.Physician;
import com.chadmaughan.cs6890.aop.sbp1.Prescription;
import com.chadmaughan.cs6890.aop.sbp1.Surgery;

/**
 * Satisfies Instructions 3a, 3b of sbp1.pdf
 * @author chadmaughan
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// common collection of shared names
		List<Name> names = new ArrayList<Name>();
		names.add(new Name("Mitt", "Middle", "Romney"));
		names.add(new Name("Barak", "Hussien", "Obama"));
		names.add(new Name("Ron", "Middle", "Paul"));
		
		// example of Name.match
		float amount1 = names.get(0).match(names.get(1));
		System.out.println("Match: " + amount1);
		float amount2 = names.get(1).match(names.get(2));
		System.out.println("Match: " + amount2);
		float amount3 = names.get(2).match(names.get(0));
		System.out.println("Match: " + amount3);
		
		// common collection of addresses
		List<Address> addresses = new ArrayList<Address>();
		Address a1 = new Address();
		a1.setStreetLine1("streetLine1");
		a1.setStreetLine2("streetLine1");
		a1.setCity("city1");
		a1.setState("state1");
		a1.setPostalCode("postalCode1");
		addresses.add(a1);

		Address a2 = new Address();
		a2.setStreetLine1("streetLine2");
		a2.setStreetLine2("streetLine2");
		a2.setCity("city2");
		a2.setState("state2");
		a2.setPostalCode("postalCode2");
		addresses.add(a2);

		Address a3 = new Address();
		a3.setStreetLine1("streetLine3");
		a3.setStreetLine2("streetLine3");
		a3.setCity("city3");
		a3.setState("state3");
		a3.setPostalCode("postalCode3");
		addresses.add(a3);

		// common collection of phone numbers
		List<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();
		PhoneNumber pn1 = new PhoneNumber("1", "801", "234-5678", "123");
		phoneNumbers.add(pn1);
		PhoneNumber pn2 = new PhoneNumber("2", "801", "234-5678", "123");
		phoneNumbers.add(pn2);
		PhoneNumber pn3 = new PhoneNumber("3", "801", "234-5678", "123");
		phoneNumbers.add(pn3);
		
		Patient patient = new Patient();
		patient.setId(1);
		patient.setBirthdate(new Date());
		patient.setGender(Patient.Gender.MALE.toString());
		patient.setPrimaryName(new Name("Joe", "Main", "Smith"));
		patient.setNames(names);
		patient.setPhoneNumbers(phoneNumbers);
		patient.setAddresses(addresses);

		// Satisfies Instructions 4a of sbp1
		// should output in this order (based on sample input above):
		// 		Sorted name: last1, first1 middle1
		// 		Sorted name: last2, first2 middle2
		// 		Sorted name: last3, first3 middle3
		List<Name> sortedNames = patient.getSortedNames();
		for(Name n : sortedNames) {
			System.out.println("Sorted name: " + n.getSortName());
		}
		
		// allergies
		Allergy al1 = new Allergy();
		al1.setAllergen("Penecillin");
		al1.setSeverity(5);
		patient.addAllergy(al1);

		Allergy al2 = new Allergy();
		al2.setAllergen("other");
		al2.setSeverity(4);
		patient.addAllergy(al2);
		
		// diagnoses
		Diagnosis d1 = new Diagnosis();
		d1.setCondition("condition");
		d1.setDate(new Date());
		d1.setNotes("notes");
		patient.addDiagnosis(d1);

		Diagnosis d2 = new Diagnosis();
		d2.setCondition("condition");
		d2.setDate(new Date());
		d2.setNotes("notes");
		patient.addDiagnosis(d2);
		
		// create a second Patient (with the same diagnosis for Instructions 4b)
		Patient otherPatient = new Patient();
		otherPatient.setPrimaryName(new Name("Joe", "Other", "Smith"));
		otherPatient.addDiagnosis(d1);
		
		// emergency contacts
		EmergencyContact e1 = new EmergencyContact();
		e1.setPrimaryName(new Name("first", "middle", "last"));
		e1.setRelationship("son");
		e1.setAddresses(new ArrayList<Address>());
		e1.setNames(names);
		e1.setPhoneNumbers(new ArrayList<PhoneNumber>());
		patient.addEmergencyContact(e1);

		EmergencyContact e2 = new EmergencyContact();
		e2.setPrimaryName(new Name("first", "middle", "last"));
		e2.setRelationship("son");
		e2.setAddresses(new ArrayList<Address>());
		e2.setNames(names);
		e2.setPhoneNumbers(new ArrayList<PhoneNumber>());
		patient.addEmergencyContact(new EmergencyContact());

		// health issues
		HealthIssue hi1 = new HealthIssue();
		hi1.setBeganOn(new Date());
		hi1.setEndedOn(new Date());
		hi1.setSymptomOrObservation("symptomOrObservation1");
		patient.addHealthIssue(hi1);

		HealthIssue hi2 = new HealthIssue();
		hi2.setBeganOn(new Date());
		hi2.setEndedOn(new Date());
		hi2.setSymptomOrObservation("symptomOrObservation2");
		patient.addHealthIssue(hi2);

		// physicians
		Physician ph1 = new Physician();
		ph1.setAddresses(addresses);
		ph1.setNames(sortedNames);
		ph1.setNationalProviderId("12345D");
		ph1.setPhoneNumbers(phoneNumbers);
		ph1.setPrimaryName(new Name("Dr", "Bob", "F", "Smith", "Jr"));
		ph1.setSpecialization("Oncology");

		ph1.addPatient(patient);
		ph1.addPatient(otherPatient);
		patient.addPhysician(ph1);

		Physician ph2 = new Physician();
		ph2.setAddresses(addresses);
		ph2.setNames(sortedNames);
		ph2.setNationalProviderId("9876A");
		ph2.setPhoneNumbers(phoneNumbers);
		ph2.setPrimaryName(new Name("Dr", "Bob", "F", "Smith", "Sr"));
		ph2.setSpecialization("Oncology");

		ph2.addPatient(patient);
		ph2.addPatient(otherPatient);
		patient.addPhysician(ph2);
		
		// Satisfies Instructions 4b of sbp1.pdf
		// should output (based on input above)
		// 		Patient with condition: condition,  Joe Main Smith
		//		Patient with condition: condition,  Joe Other Smith
		String commonCondition = "condition";
		Set<Patient> patientsWithSimilarDiagnosis = ph2.getPatientsWithDiagnosis(commonCondition);
		for(Patient p : patientsWithSimilarDiagnosis) {
			System.out.println("Patient with condition: " + commonCondition + ", " + p.getPrimaryName());
		}

		// prescriptions
		Prescription p1 = new Prescription();
		p1.setDosage("1ml");
		p1.setEndDate(new Date());
		p1.setFrequency("Daily");
		p1.setMedication("Prozac");
		p1.setStartDate(new Date());
		patient.addPrescription(p1);

		Prescription p2 = new Prescription();
		p2.setDosage("1ml");
		p2.setEndDate(new Date());
		p2.setFrequency("Daily");
		p2.setMedication("Prozac");
		p2.setStartDate(new Date());
		patient.addPrescription(p2);
		
		// surgery
		patient.addSurgery(new Surgery("type1", "notes1"));
		patient.addSurgery(new Surgery("type2", "notes2"));

	}
}
