package com.chadmaughan.cs6890.aop.project;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.chadmaughan.cs6890.aop.project.model.Mathematician;
import com.chadmaughan.cs6890.aop.project.views.MathematicianViewArrayAdapter;

public class DisplayActivity extends ListActivity{

	private final static int OPEN_LINK = 200;
	private final static int SHOW_LINK = 201;
	
	private ProjectApplication app;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);
        
        try {

    		app = (ProjectApplication) getApplication();

        	populate();
        	
	        // allow for long-click context menu
	        registerForContextMenu(this.getListView());

	        // allow for single click
	        this.getListView().setOnItemClickListener(new OnItemClickListener() {
	        	
				public void onItemClick(AdapterView<?> parent, View target, int position, long id) {
					
					// get the mathematician from the database
					Mathematician m = ((ProjectApplication) getApplication()).getMathematician(id);
					
					String url = m.getUrl();

					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity(browserIntent);
				}        	
	        });

        }
        catch(Exception e) {
        	e.printStackTrace();
        }
    }

	private void populate() {
		
        // fetch the mathematicians from the database
    	List<Mathematician> mathematicians = app.getMathematicians();

        // populate the view list
    	if(mathematicians.size() > 0) {
	        MathematicianViewArrayAdapter adapter = new MathematicianViewArrayAdapter(this, R.layout.mathematician_view, mathematicians);
	        this.getListView().setAdapter(adapter);
    	}
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {

		// get the mathematician from the database
		AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
		Mathematician m = ((ProjectApplication) getApplication()).getMathematician(adapterContextMenuInfo.id);

		menu.setHeaderTitle(m.getFullName());
		menu.add(200, OPEN_LINK, Menu.NONE, "Go to wiki page");
		menu.add(200, SHOW_LINK , Menu.NONE, "Show wiki link");
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {		

		// get the mathematician from the database
		AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();		
		Mathematician m = ((ProjectApplication) getApplication()).getMathematician(adapterContextMenuInfo.id);

		String url = m.getUrl();
		
		switch(item.getItemId()) {
		
			// open browser
			case OPEN_LINK:
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(browserIntent);
				break;
	
			// show link
			case SHOW_LINK:
				Toast t = Toast.makeText(getApplicationContext(), url, Toast.LENGTH_SHORT);
				t.show();
				break;
				
			default:
				break;
		}

		return true;
	}
}