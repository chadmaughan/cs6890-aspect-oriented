package com.chadmaughan.cs6890.aop.project.aspect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.util.Log;

/**
 * Calculates performance metrics for Activities
 * @author chadmaughan
 *
 */
public aspect Metrics {
	
	private static final String LOG = "Metrics.aj";

	pointcut calculate() : call(* Activity+.*(..)) 
		&& !call(* *.get*(..)) 
		&& !call(* *.set*(..)) 
		&& !within(Logging) 
		&& !within(Metrics)
		&& !within(Persistence);

	private Map<String, List<Double>> measurements;

	Object around() : calculate() {

		if(measurements == null) {
			measurements = new HashMap<String, List<Double>>();
		}
		
		long start = System.nanoTime();

		Object ret = proceed();

		long end = System.nanoTime();

		String method = thisJoinPoint.getSignature().toShortString();

		List<Double> ms = measurements.get(method);
		if(ms == null) {
			ms = new ArrayList<Double>();
			measurements.put(method, ms);
		}
		
		ms.add(new Double(end - start));
		Log.v(LOG, String.format("%10s", (end - start)) + " nanoseconds for: " + method);

		return ret;
	}

	pointcut metricOutput() : execution(void Activity+.onPause())
		|| execution(void Activity+.onStop())
		|| execution(void Activity+.onDestroy());

	before() : metricOutput() {}
	
	after() : metricOutput() {
		Log.v(LOG, "metric output");
		for(String key : measurements.keySet()) {
			List<Double> ms = measurements.get(key);
			if(ms.size() > 1) {
				Log.v(LOG, key);
				Log.v(LOG, "   count: " + ms.size());
				Log.v(LOG, "    mean: " + mean(ms));
				Log.v(LOG, "  median: " + median(ms));
				Log.v(LOG, "    mode: " + mode(ms));
			}
		}
	}
	
	/**
	 * Calculates the mean of the measurements
	 * @param m
	 * @return
	 */
	private static double mean(List<Double> m) {
	    double sum = 0;
	    for(Double d : m) {
	        sum += d;
	    }
	    return sum / m.size();
	}

	/**
	 * Calculates the median of the measurements
	 * @param m
	 * @return
	 */
	private static double median(List<Double> m) {
		
		// the double collection should be sorted
		Collections.sort(m);
		
	    int middle = m.size() / 2;
	    if (m.size() % 2 == 1) {
	        return m.get(middle);
	    } 
	    else {
	        return (m.get(middle-1) + m.get(middle)) / 2.0;
	    }
	}

	/**
	 * Calculates the mode of the measurements
	 * @param m
	 * @return
	 */
	private static double mode(List<Double> m) {
		double maxValue = 0;
		double maxCount = 0;

	    for (int i = 0; i < m.size(); ++i) {
	        int count = 0;
	        for (int j = 0; j < m.size(); ++j) {
	            if (m.get(j) == m.get(i)) {
	            	++count;
	            }
	        }

	        if (count > maxCount) {
	            maxCount = count;
	            maxValue = m.get(i);
	        }
	    }

	    return maxValue;
	}
}
