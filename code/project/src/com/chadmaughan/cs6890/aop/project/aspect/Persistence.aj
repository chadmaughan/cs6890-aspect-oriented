package com.chadmaughan.cs6890.aop.project.aspect;

import android.util.Log;

import com.chadmaughan.cs6890.aop.project.MainActivity;
import com.chadmaughan.cs6890.aop.project.database.MathematicianDatabaseAdapter;
import com.chadmaughan.cs6890.aop.project.model.Mathematician;

public aspect Persistence {

	private static final String LOG = "Persistence.aj";
	
	private MathematicianDatabaseAdapter database;

	// block anyone from using the database adapter directly
	declare error : call(* MathematicianDatabaseAdapter.*(..)) 
		&& !within(Persistence)
		&& !within(MathematicianDatabaseAdapter)
		: "Database adapter is only available via AOP";

	pointcut add() : execution(* MainActivity.addMathematician(..));

	before() : add() {
        
        // get the mathematician being created
        Mathematician m = (Mathematician) thisJoinPoint.getArgs()[0];
        Log.d(LOG, "Persisting mathematician: " + m.getFirstName() + " " + m.getLastName());
        
		MainActivity mainActivity = (MainActivity) thisJoinPoint.getThis();
		
        // open the database (creates the database)
		database = new MathematicianDatabaseAdapter(mainActivity);
		database.open();

		// add
		database.create(m.getFirstName(), m.getLastName(), m.getResearch(), m.getUrl());
		
		// be responsible, close it up
		database.close();

	}

	pointcut create() : execution(* MainActivity.onCreate(..));

	before() : create() {

		MainActivity mainActivity = (MainActivity) thisJoinPoint.getThis();
		
        // open the database (creates the database)
		database = new MathematicianDatabaseAdapter(mainActivity);
		database.open();
		
		// clear out any previously downloaded records
		database.empty();
		
		// be responsible, close it up
		database.close();
	}
}
