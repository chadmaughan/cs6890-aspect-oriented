package com.chadmaughan.cs6890.aop.project.aspect;

import java.util.Arrays;

import android.app.Activity;
import android.util.Log;

/**
 * @author chadmaughan
 */
public aspect Logging {

	pointcut log() : execution(* Activity+.*(..)) && !within(Logging);

	before() : log() {
		
		// use the method signature short name for the logger name
		String method = thisJoinPoint.getSignature().toShortString();
        Log.d(method, ">>>> entering " + method + ", parms=" + Arrays.toString(thisJoinPoint.getArgs()));
	}
	
	after() : log() {

		// use the method signature short name for the logger name
		String method = thisJoinPoint.getSignature().toShortString();
        Log.d(method, "<<<< exiting " + method);
	}
}
