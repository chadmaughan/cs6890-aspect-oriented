package com.chadmaughan.cs6890.aop.project;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;

import com.chadmaughan.cs6890.aop.project.model.Mathematician;

public class ProjectApplication extends Application {

	private static ProjectApplication singleton;

	private List<Mathematician> mathematicians;
	
	@Override
	public void onCreate() {
		super.onCreate();
		this.mathematicians = new ArrayList<Mathematician>();
		singleton = this;
	}

	public static ProjectApplication getInstance() {
		return singleton;
	}

	public List<Mathematician> getMathematicians() {
		return mathematicians;
	}

	public void setMathematicians(List<Mathematician> mathematicians) {
		this.mathematicians = mathematicians;
	}

	public Mathematician getMathematician(Long id) {
		return this.mathematicians.get(id.intValue());
	}
	
	public boolean addMathematician(Mathematician mathematician) {		
		return this.mathematicians.add(mathematician);
	}
}
