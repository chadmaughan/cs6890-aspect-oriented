import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.chadmaughan.cs6890.aop.sbp3.Address;
import com.chadmaughan.cs6890.aop.sbp3.Allergy;
import com.chadmaughan.cs6890.aop.sbp3.Diagnosis;
import com.chadmaughan.cs6890.aop.sbp3.EmergencyContact;
import com.chadmaughan.cs6890.aop.sbp3.HealthIssue;
import com.chadmaughan.cs6890.aop.sbp3.Name;
import com.chadmaughan.cs6890.aop.sbp3.Patient;
import com.chadmaughan.cs6890.aop.sbp3.PhoneNumber;
import com.chadmaughan.cs6890.aop.sbp3.Physician;
import com.chadmaughan.cs6890.aop.sbp3.Prescription;
import com.chadmaughan.cs6890.aop.sbp3.Surgery;
import com.chadmaughan.cs6890.aop.sbp3.session.Session;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// set up the user permissions
		Session session = Session.INSTANCE;
		session.setUserName("me");
		session.addPermission("1birthdate");
				
		// common collection of shared names
		List<Name> names = new ArrayList<Name>();
		names.add(new Name("first1", "middle1", "last1"));
		names.add(new Name("first2", "middle2", "last2"));
		names.add(new Name("first3", "middle3", "last3"));
		
		// sbp3 requirement number 4a
		// names.get(0).setFirstName("updatedFirstName");
		
		// common collection of addresses
		List<Address> addresses = new ArrayList<Address>();
		Address a1 = new Address();
		a1.setStreetLine1("streetLine1");
		a1.setStreetLine2("streetLine1");
		a1.setCity("city1");
		a1.setState("state1");
		a1.setPostalCode("postalCode1");
		addresses.add(a1);

		Address a2 = new Address();
		a2.setStreetLine1("streetLine2");
		a2.setStreetLine2("streetLine2");
		a2.setCity("city2");
		a2.setState("state2");
		a2.setPostalCode("postalCode2");
		addresses.add(a2);

		Address a3 = new Address();
		a3.setStreetLine1("streetLine3");
		a3.setStreetLine2("streetLine3");
		a3.setCity("city3");
		a3.setState("state3");
		a3.setPostalCode("postalCode3");
		addresses.add(a3);

		// common collection of phone numbers
		List<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();
		PhoneNumber pn1 = new PhoneNumber("1", "801", "234-5678", "123");
		phoneNumbers.add(pn1);
		PhoneNumber pn2 = new PhoneNumber("2", "801", "234-5678", "123");
		phoneNumbers.add(pn2);
		PhoneNumber pn3 = new PhoneNumber("3", "801", "234-5678", "123");
		phoneNumbers.add(pn3);
		
		Patient patient = new Patient(new Name("Joe", "Middle", "Smith"));
		patient.setId(1);

		// uses protected aspect
		patient.setBirthdate(new Date());
		patient.getBirthdate();
		
		// uses encrypted aspect
		patient.setGender(Patient.Gender.FEMALE.toString());
		patient.getGender();
		
		patient.setNames(names);
		patient.setPhoneNumbers(phoneNumbers);
		patient.setAddresses(addresses);

		// allergies
		Allergy al1 = new Allergy();
		al1.setAllergen("Penecillin");
		al1.setSeverity(5);
		patient.addAllergy(al1);

		Allergy al2 = new Allergy();
		al2.setAllergen("other");
		al2.setSeverity(4);
		patient.addAllergy(al2);
		
		// diagnoses
		Diagnosis d1 = new Diagnosis();
		d1.setCondition("condition");
		d1.setDate(new Date());
		d1.setNotes("notes");
		patient.addDiagnosis(d1);

		Diagnosis d2 = new Diagnosis();
		d2.setCondition("condition");
		d2.setDate(new Date());
		d2.setNotes("notes");
		patient.addDiagnosis(d2);
		
		// emergency contacts
		EmergencyContact e1 = new EmergencyContact(new Name("first", "middle", "last"));
		e1.setRelationship("son");
		e1.setAddresses(new ArrayList<Address>());
		e1.setNames(names);
		e1.setPhoneNumbers(new ArrayList<PhoneNumber>());
		patient.addEmergencyContact(e1);

		EmergencyContact e2 = new EmergencyContact(new Name("first", "middle", "last"));
		e2.setRelationship("son");
		e2.setAddresses(new ArrayList<Address>());
		e2.setNames(names);
		e2.setPhoneNumbers(new ArrayList<PhoneNumber>());
		patient.addEmergencyContact(new EmergencyContact());

		// health issues
		HealthIssue hi1 = new HealthIssue();
		hi1.setBeganOn(new Date());
		hi1.setEndedOn(new Date());
		hi1.setSymptomOrObservation("symptomOrObservation1");
		patient.addHealthIssue(hi1);

		HealthIssue hi2 = new HealthIssue();
		hi2.setBeganOn(new Date());
		hi2.setEndedOn(new Date());
		hi2.setSymptomOrObservation("symptomOrObservation2");
		patient.addHealthIssue(hi2);

		// physicians
		patient.addPhysician(new Physician());
		patient.addPhysician(new Physician());
		
		// prescriptions
		Prescription p1 = new Prescription();
		p1.setDosage("1ml");
		p1.setEndDate(new Date());
		p1.setFrequency("Daily");
		p1.setMedication("Prozac");
		p1.setStartDate(new Date());
		patient.addPrescription(p1);

		Prescription p2 = new Prescription();
		p2.setDosage("1ml");
		p2.setEndDate(new Date());
		p2.setFrequency("Daily");
		p2.setMedication("Prozac");
		p2.setStartDate(new Date());
		patient.addPrescription(p2);
		
		// surgery
		patient.addSurgery(new Surgery("type1", "notes1"));
		patient.addSurgery(new Surgery("type2", "notes2"));

	}
}
