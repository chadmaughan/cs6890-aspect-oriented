package com.chadmaughan.cs6890.aop.sbp3.aspect;

import java.awt.event.MouseEvent;


public aspect EditAddressClick {

	pointcut click(MouseEvent mouseEvent) : execution(* *.mouseClicked(*)) && args(mouseEvent);

	before(MouseEvent mouseEvent) : click(mouseEvent) {
		// do whatever you want with the mouse event
		System.out.println("MouseEvent");
	}
}
