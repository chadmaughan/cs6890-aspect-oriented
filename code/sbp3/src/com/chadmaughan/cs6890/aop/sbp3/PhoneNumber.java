package com.chadmaughan.cs6890.aop.sbp3;

import com.chadmaughan.cs6890.aop.sbp3.util.Util;

@SuppressWarnings("serial")
public class PhoneNumber extends Base implements Comparable<PhoneNumber> {

	public enum type {
		HOME, WORK, MOBILE
	}

	String areaCode;
	String exchange;
	String detailNumber;
	String extension;

	public PhoneNumber() {}
	
	public PhoneNumber(String exchange, String areaCode, String detailNumber, String extension) {
		this.exchange = exchange;
		this.areaCode = areaCode;
		this.detailNumber = detailNumber;
		this.extension = extension;
	}
	
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public String getDetailNumber() {
		return detailNumber;
	}

	public void setDetailNumber(String detailNumber) {
		this.detailNumber = detailNumber;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getFormattedNumber() {
		return exchange + " (" + areaCode + ") " + detailNumber + " ext " + extension; 
	}

	@Override
	public int compareTo(PhoneNumber phoneNumber) {
		return getFormattedNumber().compareTo(this.getFormattedNumber());
	}
	
	public float match(PhoneNumber phoneNumber) {
		return Util.computeLevenshteinDistance(phoneNumber.getFormattedNumber(), this.getFormattedNumber());
	}
}
