package com.chadmaughan.cs6890.aop.sbp3.aspect;

import com.chadmaughan.cs6890.aop.sbp3.Name;

/**
 * Satisfies Instructions 4a on sbp3.pdf
 */
public aspect Constraints {

	// block anyone from using the individual setters for name parts
	declare error : call(void Name.set*Name(String)) && !within(Name)
		: "Names should only be updated from the Name.update() method";
}
