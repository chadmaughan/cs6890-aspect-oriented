package com.chadmaughan.cs6890.aop.sbp3.aspect;

import java.awt.event.MouseEvent;

import com.chadmaughan.cs6890.aop.sbp3.ui.Ui;


public aspect EditPersonClick {

	pointcut click(MouseEvent mouseEvent) : execution(* *.mouseClicked(*)) && args(mouseEvent);

	before(MouseEvent mouseEvent) : click(mouseEvent) {
		System.out.println("repopulating list of persons");
		Ui.updatePersons();
	}
}
