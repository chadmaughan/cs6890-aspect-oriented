package com.chadmaughan.cs6890.aop.sbp3;

@SuppressWarnings("serial")
public class EmergencyContact extends Person {

	String relationship;

	public EmergencyContact() {}
	
	public EmergencyContact(Name primaryName) {
		super(primaryName);
	}
	
	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
}
