package com.chadmaughan.cs6890.aop.sbp3.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class PhoneNumberForm extends JDialog {

	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	private JTextField txtExchange;
	private JTextField txtAreaCode;
	private JTextField txtDetailNumber;
	private JTextField txtExtension;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PhoneNumberForm dialog = new PhoneNumberForm();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PhoneNumberForm() {
		
		setTitle("Add/Edit Phone Number");
		setBounds(100, 100, 319, 242);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblExchange = new JLabel("Exchange:");
		lblExchange.setHorizontalAlignment(SwingConstants.RIGHT);
		lblExchange.setBounds(25, 22, 100, 16);
		contentPanel.add(lblExchange);

		JLabel lblAreaCode = new JLabel("Area Code:");
		lblAreaCode.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAreaCode.setBounds(25, 52, 100, 16);
		contentPanel.add(lblAreaCode);
		
		JLabel lblDetailNumber = new JLabel("Detail Number:");
		lblDetailNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDetailNumber.setBounds(25, 82, 100, 16);
		contentPanel.add(lblDetailNumber);
		
		JLabel lblExtension = new JLabel("Extension:");
		lblExtension.setHorizontalAlignment(SwingConstants.RIGHT);
		lblExtension.setBounds(25, 114, 100, 16);
		contentPanel.add(lblExtension);
		
		txtExchange = new JTextField();
		txtExchange.setBounds(137, 16, 150, 28);
		contentPanel.add(txtExchange);
		txtExchange.setColumns(10);

		txtAreaCode = new JTextField();
		txtAreaCode.setBounds(137, 46, 150, 28);
		contentPanel.add(txtAreaCode);
		txtAreaCode.setColumns(10);
		
		txtDetailNumber = new JTextField();
		txtDetailNumber.setBounds(137, 76, 150, 28);
		contentPanel.add(txtDetailNumber);
		txtDetailNumber.setColumns(10);
		
		txtExtension = new JTextField();
		txtExtension.setBounds(137, 108, 150, 28);
		contentPanel.add(txtExtension);
		txtExtension.setColumns(10);
		
		txtExtension.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent me) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
