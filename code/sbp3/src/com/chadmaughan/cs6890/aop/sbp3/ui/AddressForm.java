package com.chadmaughan.cs6890.aop.sbp3.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class AddressForm extends JDialog {

	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	private JTextField txtStreetLine1;
	private JTextField txtStreetLine2;
	private JTextField txtCity;
	private JTextField txtState;
	private JTextField txtPostalCode;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AddressForm dialog = new AddressForm();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddressForm() {
		setTitle("Add/Edit Address");
		setBounds(100, 100, 319, 242);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblStreetLine1 = new JLabel("Street Line 1:");
		lblStreetLine1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblStreetLine1.setBounds(25, 22, 100, 16);
		contentPanel.add(lblStreetLine1);

		JLabel lblStreetLine2 = new JLabel("Street Line 2:");
		lblStreetLine2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblStreetLine2.setBounds(25, 52, 100, 16);
		contentPanel.add(lblStreetLine2);
		
		JLabel lblCity = new JLabel("City:");
		lblCity.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCity.setBounds(25, 82, 100, 16);
		contentPanel.add(lblCity);
		
		JLabel lblState = new JLabel("State:");
		lblState.setHorizontalAlignment(SwingConstants.RIGHT);
		lblState.setBounds(25, 114, 100, 16);
		contentPanel.add(lblState);
		
		JLabel lblPostalCode = new JLabel("Postal Code:");
		lblPostalCode.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPostalCode.setBounds(25, 143, 100, 16);
		contentPanel.add(lblPostalCode);

		txtStreetLine1 = new JTextField();
		txtStreetLine1.setBounds(137, 16, 150, 28);
		contentPanel.add(txtStreetLine1);
		txtStreetLine1.setColumns(10);

		txtStreetLine2 = new JTextField();
		txtStreetLine2.setBounds(137, 46, 150, 28);
		contentPanel.add(txtStreetLine2);
		txtStreetLine2.setColumns(10);
		
		txtCity = new JTextField();
		txtCity.setBounds(137, 76, 150, 28);
		contentPanel.add(txtCity);
		txtCity.setColumns(10);
		
		txtState = new JTextField();
		txtState.setBounds(137, 108, 150, 28);
		contentPanel.add(txtState);
		txtState.setColumns(10);
		
		txtPostalCode = new JTextField();
		txtPostalCode.setBounds(137, 137, 150, 28);
		contentPanel.add(txtPostalCode);
		
		txtPostalCode.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent me) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}