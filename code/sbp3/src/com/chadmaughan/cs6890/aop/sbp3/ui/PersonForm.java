package com.chadmaughan.cs6890.aop.sbp3.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.chadmaughan.cs6890.aop.sbp3.Person;

public class PersonForm extends JDialog {

	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	private JTextField txtPrefix;
	private JTextField txtFirstName;
	private JTextField txtMiddleName;
	private JTextField txtLastName;
	private JTextField txtSuffix;

	private Person person;

	/**
	 * Create the dialog.
	 */
	public PersonForm() {
		create();
	}
	
	public PersonForm(Person person) {
		this.person = person;
		create();
	}
	
	private void create() {
		setTitle("Add/Edit Person");
		setBounds(100, 100, 319, 242);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblPrefix = new JLabel("Prefix:");
		lblPrefix.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrefix.setBounds(25, 22, 100, 16);
		contentPanel.add(lblPrefix);

		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFirstName.setBounds(25, 52, 100, 16);
		contentPanel.add(lblFirstName);
		
		JLabel lblMiddleName = new JLabel("Middle Name:");
		lblMiddleName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMiddleName.setBounds(25, 82, 100, 16);
		contentPanel.add(lblMiddleName);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLastName.setBounds(25, 114, 100, 16);
		contentPanel.add(lblLastName);
		
		JLabel lblSuffix = new JLabel("Suffix:");
		lblSuffix.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSuffix.setBounds(25, 143, 100, 16);
		contentPanel.add(lblSuffix);

		txtPrefix = new JTextField();
		txtPrefix.setBounds(137, 16, 150, 28);
		if(person.getPrimaryName().getSalutation() != null)
			txtPrefix.setText(person.getPrimaryName().getSalutation());
		contentPanel.add(txtPrefix);
		txtPrefix.setColumns(10);

		txtFirstName = new JTextField();
		txtFirstName.setBounds(137, 46, 150, 28);
		if(person.getPrimaryName().getFirstName() != null)
			txtFirstName.setText(person.getPrimaryName().getFirstName());
		contentPanel.add(txtFirstName);
		txtFirstName.setColumns(10);
		
		txtMiddleName = new JTextField();
		txtMiddleName.setBounds(137, 76, 150, 28);
		if(person.getPrimaryName().getMiddleName() != null)
			txtMiddleName.setText(person.getPrimaryName().getMiddleName());
		contentPanel.add(txtMiddleName);
		txtMiddleName.setColumns(10);
		
		txtLastName = new JTextField();
		txtLastName.setBounds(137, 108, 150, 28);
		if(person.getPrimaryName().getLastName() != null)
			txtLastName.setText(person.getPrimaryName().getLastName());
		contentPanel.add(txtLastName);
		txtLastName.setColumns(10);
		
		txtSuffix = new JTextField();
		txtSuffix.setBounds(137, 137, 150, 28);
		if(person.getPrimaryName().getSuffix() != null)
			txtSuffix.setText(person.getPrimaryName().getSuffix());
		contentPanel.add(txtSuffix);
		
		
		txtSuffix.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent me) {
						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent me) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
