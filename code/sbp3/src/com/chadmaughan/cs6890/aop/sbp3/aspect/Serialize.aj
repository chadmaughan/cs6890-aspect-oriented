package com.chadmaughan.cs6890.aop.sbp3.aspect;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;

import com.chadmaughan.cs6890.aop.sbp3.Person;
import com.chadmaughan.cs6890.aop.sbp3.session.Session;

/**
 * Satisfies the Instructions requirement 3a
 * 		Can serialize the set of known person objects (and all connected objects) to file before the 
 * 		program exits and re‐load that file when the program begins (if the file exists). Change you 
 * 		program to only create some sample persons if no persons were loaded.
 * 
 * @author chadmaughan
 *
 */
public aspect Serialize {

	pointcut serialize() : execution(* *.main(..));

	// note: only works on execution (not call)
	pointcut addPerson(Person person) : target(person) && execution(Person.new(..));
	
	private String FILENAME = "user";
	private String PATH = System.getProperty("java.io.tmpdir");

	/**
	 * Adds to the session after every Person instantiation
	 * @param person
	 */
	after(Person person) : addPerson(person) {
    	if(Session.INSTANCE.getPersons() == null)
    		Session.INSTANCE.setPersons(new HashSet<Person>());
    	Session.INSTANCE.getPersons().add(person);
    	System.out.println("Added person to session: " + person.getPrimaryName());
	}

	before() : serialize() {

		System.out.println("System startup");

		// look for serialized users
		File dir = new File(PATH);
		System.out.println("Checking for serialized persons with absolute path: " + PATH);


		String[] children = dir.list();
		if (children == null) {
			System.out.println("No serialized persons");
		} 
		else {
		    for (int i = 0; i < children.length; i++) {
		    	if(children[i].startsWith(FILENAME)) {
		    		try {
			    		File serializedPerson = new File(children[i]);
			    		FileInputStream fis = new FileInputStream(PATH + serializedPerson);
						ObjectInputStream ois = new ObjectInputStream(fis);	

						Person person = (Person) ois.readObject();
						
						// don't import unless they have a primary name
						if(person.getPrimaryName() != null) {
							Session.INSTANCE.addSessionPerson(person);
							System.out.println("Retrived serialized person: " + person.getPrimaryName());
						}
						
						ois.close();
		    		}
					catch(FileNotFoundException fnfe) {
						fnfe.printStackTrace();
					}
					catch(ClassNotFoundException cnfe) {
						cnfe.printStackTrace();
					}
					catch(IOException ioe) {
						ioe.printStackTrace();
					}
		    	}
		    }
		}
	}
	
	after() : serialize() {
		
		System.out.println("System shutdown");
		
		try {

			int count = 1;
			if(Session.INSTANCE.getPersons() != null) {
				for(Person p : Session.INSTANCE.getPersons()) {
	
					File serializedPerson = new File(PATH + "user." + count);
					
					System.out.println("Serializing person: " + p.getPrimaryName());
					
					FileOutputStream fos = new FileOutputStream(serializedPerson);
	
					ObjectOutputStream oos = new ObjectOutputStream(fos);			
					oos.writeObject(p);
					oos.close();
	
					count++;
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
