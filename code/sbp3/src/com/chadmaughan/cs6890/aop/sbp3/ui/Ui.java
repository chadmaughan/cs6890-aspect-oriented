package com.chadmaughan.cs6890.aop.sbp3.ui;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.chadmaughan.cs6890.aop.sbp3.Address;
import com.chadmaughan.cs6890.aop.sbp3.Person;
import com.chadmaughan.cs6890.aop.sbp3.session.Session;

public class Ui {

	private JFrame frmSbp;

	private PersonForm personForm;
	int selectedGlobal = 0;
	public static DefaultListModel personModelList;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ui window = new Ui();
					window.frmSbp.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ui() {
		initialize();
	}

	
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSbp = new JFrame();
		frmSbp.setResizable(false);
		frmSbp.setTitle("SBP3");
		frmSbp.setBounds(100, 100, 719, 594);
		frmSbp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSbp.getContentPane().setLayout(null);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(261, 0, 457, 572);
		frmSbp.getContentPane().add(tabbedPane);
		
		JTabbedPane phoneNumberPanel = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Phone Numbers", null, phoneNumberPanel, null);
		
		final JList phoneNumberList = new JList();
		phoneNumberPanel.add(phoneNumberList);
		
		JTabbedPane addressPanel = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Addresses", null, addressPanel, null);
		
		final JList addressList = new JList();
		addressPanel.add(addressList);
		
		JPanel personPanel = new JPanel();
		personPanel.setBounds(6, 6, 254, 502);
		frmSbp.getContentPane().add(personPanel);
		
		JLabel lblPerson = new JLabel("Person");
		personPanel.add(lblPerson);
		
		JList list = new JList();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
		        
				JList list = (JList) e.getSource();
		        DefaultListModel model = (DefaultListModel) list.getModel();
		        
		        int selected = list.getSelectedIndex();
		        selectedGlobal = selected;
		        Person person = (Person) model.get(selected);
		        System.out.println(selected + ": " + person.getPrimaryName());

				DefaultListModel addressModel = new DefaultListModel();
		        for(Address a : person.getAddresses()) {
		        	addressModel.addElement(a.toString());
		        	System.out.println("added address: " + a.toString());
		        }

		        addressList.setModel(addressModel);		        
		    }
		});
				
		personModelList = new DefaultListModel();
		if(Session.INSTANCE.getPersons() != null) {
			for(Person p : Session.INSTANCE.getPersons()) {
			    personModelList.addElement(p);
			}
		}
		
		list.setModel(personModelList);
		personPanel.add(list);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setBounds(6, 520, 254, 39);
		frmSbp.getContentPane().add(buttonsPanel);
		
		JButton btnAddPerson = new JButton("Add");
		btnAddPerson.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				personForm = new PersonForm();
				personForm.setVisible(true);
			}
		});
		buttonsPanel.add(btnAddPerson);
		
		JButton btnDeletePerson = new JButton("Delete");
		buttonsPanel.add(btnDeletePerson);
		
		JButton btnEditPerson = new JButton("Edit");
		btnEditPerson.addMouseListener(new EditClick());
		buttonsPanel.add(btnEditPerson);
	}

	public static void updatePersons() {
		if(Session.INSTANCE.getPersons() != null) {
			for(Person p : Session.INSTANCE.getPersons()) {
			    personModelList.addElement(p);
			}
		}
	}
	
	class EditClick extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent me) {
			if(selectedGlobal == 0) {
				JOptionPane.showMessageDialog(frmSbp, "You must select a person", "Error", JOptionPane.DEFAULT_OPTION);
			}
			else {
				PersonForm personForm = new PersonForm(((Person) personModelList.get(selectedGlobal)));
				personForm.setVisible(true);
			}
		}
	}
}
