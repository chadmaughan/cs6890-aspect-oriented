package com.chadmaughan.cs6890.aop.sbp3;

import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("serial")
public class Physician extends Person {

	String nationalProviderId;
	String specialization;

	Set<Patient> patients;
	
	public Physician() {}
	
	public String getNationalProviderId() {
		return nationalProviderId;
	}

	public void setNationalProviderId(String nationalProviderId) {
		this.nationalProviderId = nationalProviderId;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public Set<Patient> getPatients() {
		return patients;
	}

	public void setPatients(Set<Patient> patients) {
		this.patients = patients;
	}

	public boolean addPatient(Patient patient) {
		if (patients == null)
			patients = new HashSet<Patient>();
		return patients.add(patient);
	}

	public boolean removePatient(Patient patient) {
		return patients.remove(patient);
	}

}
