package Examples;

public class T extends S {
	
	private int myB;
	private int myC;
	
	public T()
	{
		clear();
	}
	
	public T(int b, int c)
	{
		myB = b;
		myC = c;
	}
	
	public String getB() { return "T Value of " + myB; }
	public void setB(String b) { myB = Integer.parseInt(b); }
	
	@Getter
	public int getC() { return myC; }
	
	@Setter
	public void setC(int c) { myC = c; }
	
	public void clear()
	{
		super.clear();
		myB = 100;		
		myC = 200;
	}
	
	public R3 doIt(int x) {
		return new R3(myC + x);
	}

}
