package Examples;

public class Tester {

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		
		Q q0 = new P();
		
		R2 result = q0.doIt(234);
		System.out.println("q0.doIt(234) --> "+result.toString());
		
		S s1 = new S(100,"s1");
        System.out.println("s1.getA() --> " + s1.getA());
        System.out.println("s1.getB() --> " + s1.getB());
		result = s1.doIt(234);
		System.out.println("s1.doIt(234) --> " + result.toString());
		
		S s2 = new T(400, 200);
        System.out.println("s2.getA() --> " + s2.getA());
        System.out.println("s2.getB() --> " + s2.getB());
		result = s2.doIt(234);
		System.out.println("s2.doIt(234) --> " + result.toString());
		
		T t3 = new T(500, 300);
        System.out.println("t3.getA() --> " + t3.getA());
        System.out.println("t3.getB() --> " + t3.getB());
        System.out.println("t3.getC() --> " + t3.getC());
		result = t3.doIt(234);
		System.out.println("t3.doIt(234) --> " + result.toString());

		S s4 = t3;
        System.out.println("s4.getA() --> " + s4.getA());
        System.out.println("s4.getB() --> " + s4.getB());
		result = s4.doIt(234);
		System.out.println("s4.doIt(234) --> " + result.toString());
	}

}

