package Examples;

public class R3 extends R2 {

	public R3(int x) {
		super(x);
	}

	@Override
	public String toString()
	{
		return "R3 object, with myX=" + super.getMyX();
	}
}
