package Examples;

public class R2 extends R1 {
	private int myX;
	public R2(int x) { myX = x; }
	
	public int getMyX() { return myX; }
	public void setMyX(int x) { myX = x; }
	
	@Override
	public String toString()
	{
		return "R2 object, with myX=" + myX;
	}
}
