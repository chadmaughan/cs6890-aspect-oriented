package Examples;

public class S extends P {
	
	private int myA;
	private String myB;
	
	public S()
	{
		clear();
	}
	
	public S(int a, int b)
	{
		myA = a;
		myB = "SValue for #" + b;
	}
	
	public S(int a, String b)
	{
		myA = a;
		myB = b;
	}

	@Getter
	public int getA() { return myA; }

	@Setter
	public void setA(int a) { myA = a; }
	
	@Getter
	public String getB() { return myB; }

	@Setter
	public void setB(String b) { myB = b; }
	
	public void clear()
	{
		myA = 100;
		myB = "Default Value";		
	}
	
	@Override
	public R2 doIt(int x) {
		return new R2(myA + x);
	}
}
