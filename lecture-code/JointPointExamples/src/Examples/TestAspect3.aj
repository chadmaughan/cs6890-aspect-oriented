package Examples;

public aspect TestAspect3 {
	
	pointcut CallsToGetter() : call(@Getter * *.*(..));

	Object around() : CallsToGetter() {
	      System.out.print("Before Getter " +
		          thisJoinPointStaticPart.getSignature().getName());
		  System.out.println(" in class: " +
		          thisJoinPointStaticPart.getSignature().getDeclaringType().getName());
		      
		  Object result = proceed();
		      
	      System.out.print("After Getter " +
		          thisJoinPointStaticPart.getSignature().getName());
		  System.out.println(" in class: " +
		          thisJoinPointStaticPart.getSignature().getDeclaringType().getName());
		        		  
		  return result;    
	}
	
}
