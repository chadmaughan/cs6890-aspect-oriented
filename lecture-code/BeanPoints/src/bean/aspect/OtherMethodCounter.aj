package bean.aspect;


public aspect OtherMethodCounter extends AbstractCounter {

	pointcut increment(): call(* bean.*.*(..));

	@Override
	protected String getCounterName() {
		return OtherMethodCounter.class.getName();
	}
}
