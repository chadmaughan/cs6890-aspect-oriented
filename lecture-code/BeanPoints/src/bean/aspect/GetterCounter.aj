package bean.aspect;


// 'this' target is the object executed
// treat an aspect as a class
// we can change how often an instance of an aspect is created
public aspect GetterCounter extends AbstractCounter perthis(execution(* bean.*.get*(..))) {

	pointcut increment(): call(* *.get*(..));

	@Override
	protected String getCounterName() {
		return GetterCounter.class.getName();
	}

}
