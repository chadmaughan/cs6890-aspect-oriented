package bean.aspect;


public aspect SetterCounter extends AbstractCounter {

	pointcut increment(): call(void bean.*.set*(..));

	@Override
	protected String getCounterName() {
		return SetterCounter.class.getName();
	}
}
