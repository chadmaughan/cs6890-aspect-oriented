package bean.aspect;

public abstract aspect AbstractCounter {

	// controls the order that the aspects are woven in
	declare precedence : GetterCounter, SetterCounter, OtherMethodCounter;

	protected int count = 0;
	protected int exceptionCount = 0;

	pointcut increment();

	pointcut display(): execution(* bean.*.main(..));

	after() returning: increment() {
		count++;
	}

	after() throwing: increment() {
		exceptionCount++;
	}

	after(): display() {
		System.out.println("count: " + count);
		System.out.println("exception count: " + exceptionCount);
	}
	
	protected abstract String getCounterName();
}