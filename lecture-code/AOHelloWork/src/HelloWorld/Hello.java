package HelloWorld;

public class Hello {

	/**
	 * @param args
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{		
		sayHello();
		sayGoodDay();
		sayGoodMorning();
		sayGoodAfternoon();
		sayGoodEvening();
		sayGoodNight();
		sayGoodBye();
		String tmp = sayHelloSomething("Joe");
		System.out.println("tmp="+tmp);
	}
	
	public static void sayHello() {
		System.out.print("Hello Hello");
	}
	
	public static void sayGoodDay() {
		System.out.print("Good Day");
	}

	public static void sayGoodMorning() {
		System.out.print("Good Morning");
	}

	public static void sayGoodAfternoon() {
		System.out.print("Good Afternoon");
	}

	public static void sayGoodEvening() {
		System.out.print("Good Evening");
	}

	public static void sayGoodNight() {
		System.out.print("Good Night");
	}

	public static void sayGoodBye() {
		System.out.print("Good Bye");
	}
	
	public static String sayHelloSomething(String something)
	{
		System.out.print("Hello " + something);
		return something;
	}
}
