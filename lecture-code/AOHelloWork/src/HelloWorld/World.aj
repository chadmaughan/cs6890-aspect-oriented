package HelloWorld;

public aspect World {
	
	
    pointcut greeting() : execution(void Hello.sayGood*(..)); 

    pointcut greeting2(String something) :
    	execution(String Hello.say*(String)) &&
    	args(something); 

    /*
    before() : greeting() {
    	System.out.print("Hey there -- ");
    }
    
    after() returning() : greeting() { 
        System.out.println(" World!"); 
    }
    */
    
    String around(String something) : greeting2(something)
    {
    	System.out.print("Hey there -- ");

    	String result = proceed("John");
    	
        System.out.println(" World!");
        
        return result;
    }
    
}
