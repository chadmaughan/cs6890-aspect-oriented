package Example03;

import AppLayer.*;

public aspect OperationCounter
{
	private	static int count = 0;
	
	pointcut BinaryOperations() : execution(Double BinaryExpression+.Evaluate(..));
	pointcut Evaluations() : execution(Double *.Evaluate(..));
	pointcut RootEvaluation() : Evaluations() && !cflowbelow(Evaluations());	
	
	after() returning : BinaryOperations()
	{
		count++;
	}
	
	before() :  RootEvaluation()
	{
		ResetCount();
	}
	
	public static int GetCount()
	{
		return count;
	}
	
	public static void ResetCount()
	{
		count = 0;
	}
}
