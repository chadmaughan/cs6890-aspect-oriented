package Example06;

import AppLayer.*;

public aspect OperationCounter
{
	private	int count = 0;
	
	pointcut BinaryOperations() : execution(Double BinaryExpression+.Evaluate(..));
	pointcut Evaluations() : execution(Double *.Evaluate(..));
	pointcut RootEvaluation() : Evaluations() && !cflowbelow(Evaluations());
	pointcut CalculatorEval() : call(* Calculator.Evaluate(..));
	
	after() returning : CalculatorEval()
	{
		System.out.println("\t\t\t# of ops  = " + GetCount());
	}	

	after() returning : BinaryOperations()
	{
		count++;
	}
	
	before() :  RootEvaluation()
	{
		ResetCount();
	}
	
	public int GetCount()
	{
		return count;
	}
	
	public void ResetCount()
	{
		count = 0;
	}
}
