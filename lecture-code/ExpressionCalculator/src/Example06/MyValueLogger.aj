package Example06;

public aspect MyValueLogger
{
	pointcut PrivateSets() : set(private * *.myValue);

	after() returning : PrivateSets()
	{
		String target = thisJoinPoint.getThis().getClass().getName();
		String signature = thisJoinPointStaticPart.getSignature().getName();
		System.out.println("set " + target + "." + signature);		
	}
	

}
