package Example06;

import AppLayer.*;

public class CalculatorTestor {

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		
		Expression ex;
		Double result;
		String testCase;
		
		Calculator calculator = new Calculator();

		// Run some tests
		ExpressionParser ep = new ExpressionParser();
		
		System.out.println("\nTest 1\n");
		testCase = "3.68";	
		ep.Parse(testCase);
		// System.out.println("\t\t\tresult    = " + result.toString());

		System.out.println("\nTest 2\n");
		testCase = "x"; 
		//System.out.println("\t\t\tresult    = " + result.toString());

		System.out.println("\nTest 3\n");
		testCase = "3.0*45.7"; 
		// System.out.println("\t\t\tresult    = " + result.toString());

		System.out.println("\nTest 4\n");
		testCase = "(3.0 * 45.7) + 20"; 
		// System.out.println("\t\t\tresult    = " + result.toString());
		
		System.out.println("\nTest 5\n");
		testCase = "((259.2-3.6)*4.5) + (6.0 - 4.4)";
		//System.out.println("\t\t\tresult    = " + result.toString());
	}


}
