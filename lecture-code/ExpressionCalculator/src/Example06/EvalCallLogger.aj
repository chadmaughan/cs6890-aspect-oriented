package Example06;

public aspect EvalCallLogger
{
	
	pointcut EvalCalls(Object caller, Object called):
		call(* *.Evaluate(..)) &&
		this(caller) &&
		target(called);
	
	before(Object caller, Object called): EvalCalls(caller,called)
	{
		System.out.println("\nCaller is "+caller+
				" of type "+caller.getClass());
		System.out.println("Called object is "+called+
				" of type "+called.getClass());
	}
	

}
