package Example04;

import AppLayer.*;

public class CalculatorTestor {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Calculator calculator = new Calculator();

		// Run some tests
		System.out.println("\nTest 1\n");
		Expression ex = new Add
							   (
							    	new Multiple(
							    			new Constant(3.0),
							    			new Constant(45.7)),
							    	new Constant(20)
							   );
		
		System.out.println("Evaluating expression: " + ex.toString());
		Double result = calculator.Evaluate(ex);
		System.out.println("\t\t\tresult    = " + result.toString());
		System.out.println("\t\t\t# of ops  = " + OperationCounter.GetCount());

		System.out.println("\nTest 2\n");
		ex = new Add
				   (
				    	new Multiple(
				    			new Subtract(
				    					new Constant(259.2),
				    					new Symbol("x",3.6)),
				    			new Constant(4.5)),
				    	new Subtract(
				    			new Constant(6.0),
				    			new Symbol("y",4.4))
				   );

		System.out.println("Evaluating expression: " + ex.toString());
		result = calculator.Evaluate(ex);
		System.out.println("\t\t\tresult    = " + result.toString());
		System.out.println("\t\t\t# of ops  = " + OperationCounter.GetCount());

	}


}
