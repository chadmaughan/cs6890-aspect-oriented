package Example04;

import AppLayer.*;

public aspect OperationCounter
{
	private	static int count = 0;
	
	pointcut BinaryOperations() : execution(Double BinaryExpression+.Evaluate(..));
	pointcut Evaluations() : execution(Double *.Evaluate(..));
	pointcut RootEvaluation() : Evaluations() && !cflowbelow(Evaluations());
	
	pointcut EvalCalls(Object caller, Object called):
		call(* *.Evaluate(..)) &&
		this(caller) &&
		target(called);
	
	after() returning : BinaryOperations()
	{
		count++;
	}
	
	before() :  RootEvaluation()
	{
		ResetCount();
	}
	
	before(Object caller, Object called): EvalCalls(caller,called)
	{
		System.out.println("\nCaller is "+caller+
				" of type "+caller.getClass());
		System.out.println("Called object is "+called+
				" of type "+called.getClass());
	}
	
	
	public static int GetCount()
	{
		return count;
	}
	
	public static void ResetCount()
	{
		count = 0;
	}
}
