package Example02;

import AppLayer.*;

public aspect OperationCounter
{
	private	static int	count = 0;
	
	pointcut BinaryOperations() : execution(Double BinaryExpression+.Evaluate(..));
	pointcut EvalCalls() : execution(Double *.Evaluate(..));
	pointcut RootEvals() : EvalCalls() && !cflowbelow(EvalCalls());
	
	before() : RootEvals()
	{
		ResetCount();
	}
	
	after() returning : BinaryOperations()
	{
		count++;
	}
	
	public static int GetCount()
	{
		return count;
	}
	
	public static void ResetCount()
	{
		count = 0;
	}
}
