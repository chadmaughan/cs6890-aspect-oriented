package Example05;

import AppLayer.*;

public aspect OperationCounter
{
	private	int count = 0;
	
	pointcut BinaryOperations() : execution(Double BinaryExpression+.Evaluate(..));
	pointcut Evaluations() : execution(Double *.Evaluate(..));
	pointcut RootEvaluation() : Evaluations() && !cflowbelow(Evaluations());
	
	pointcut EvalCalls(Object caller, Object called):
		call(* *.Evaluate(..)) &&
		this(caller) &&
		target(called);
	
	pointcut PrivateSets() : set(private * *.myValue);
	pointcut CalculatorEval() : call(* Calculator.Evaluate(..));
	
	after() returning : CalculatorEval()
	{
		System.out.println("\t\t\t# of ops  = " + GetCount());
	}
	
	after() returning : BinaryOperations()
	{
		count++;
	}
	
	before() :  RootEvaluation()
	{
		ResetCount();
	}
	
	before(Object caller, Object called): EvalCalls(caller,called)
	{
		System.out.println("\nCaller is "+caller+
				" of type "+caller.getClass());
		System.out.println("Called object is "+called+
				" of type "+called.getClass());
	}
	
	after() returning : PrivateSets()
	{
		String target = thisJoinPoint.getThis().getClass().getName();
		String signature = thisJoinPointStaticPart.getSignature().getName();
		System.out.println("set " + target + "." + signature);		
	}
	
	public int GetCount()
	{
		return count;
	}
	
	public void ResetCount()
	{
		count = 0;
	}
}
