package AppLayer;

public class Token
{
	public enum Type { Constant, Symbol, Add, Subtract, Multiple, Divide, Subexpression, Illegal };
	
    private Type myType;
	private String myValue;
	
	public Token(Type type, String value)
	{
		myType = type;
		myValue = value.trim();
	}
	
	public Type Type()
	{
		return myType;
	}
	
	public String Value()
	{
		return myValue;
	}
	
	public Boolean IsAnOperator()
	{
		return myType == Type.Add ||
			   myType == Type.Subtract ||
			   myType == Type.Multiple ||
			   myType == Type.Divide;
	}
}
