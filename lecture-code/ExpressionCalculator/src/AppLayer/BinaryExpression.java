package AppLayer;

public abstract class BinaryExpression extends Expression
{
	private Expression op1;
	private Expression op2;
	
	public BinaryExpression(Expression left, Expression right)
	{
		op1 = left;
		op2 = right;
	}

	protected Expression Left()
	{
		return op1;
	}
	
	protected Expression Right()
	{
		return op2;
	}
}
