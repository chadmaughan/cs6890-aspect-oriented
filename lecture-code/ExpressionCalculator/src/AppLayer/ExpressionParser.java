package AppLayer;

public class ExpressionParser
{

	private enum Mode {LookingForLeft, LookingForOperator, LookingForRight, IllegalExpression};
	
	public Expression Parse(String expString)
	{
		expString = expString.replace(" ", "");
		expString = expString.replace("\t","");
		return Parse(new RawExpression(expString));
	}
	
	public Expression Parse(RawExpression raw)
	{
		Expression result = null;
		Expression left = null;
		Token operator = null;
		Expression right = null;
		Mode currentMode = Mode.LookingForLeft;
		
		try
		{

	
			while (raw.MoreToParse() && currentMode!=Mode.IllegalExpression)
			{
				switch (currentMode)
				{
					case LookingForLeft:
						left = ParseOperand(raw);
						currentMode = Mode.LookingForOperator;
						break;
					case LookingForOperator:
						operator = ParseOperator(raw);
						if (operator!=null)
							currentMode = Mode.LookingForRight;
						else
							currentMode = Mode.IllegalExpression;
						break;
					case LookingForRight:
						right = ParseOperand(raw);
						if (right!=null)
						{
							switch (operator.Type())
							{
								case Add:
									left = new Add(left, right);
									break;
								case Subtract:
									left = new Subtract(left, right);
									break;
								case Multiple:
									left = new Multiple(left, right);
									break;
								case Divide:
									right = new Divide(left, right);
									break;
							}
							currentMode = Mode.LookingForOperator;
						}
						else
							currentMode = Mode.IllegalExpression;
						break;
				}
			}
		}
		catch(Exception err)
		{
			currentMode=Mode.IllegalExpression;
		}
		
		// If looking for an operator, than the left is the valid expression
		if (currentMode==Mode.LookingForOperator)
			result = left;
		
		return result;
	}
		
	private Expression ParseOperand(RawExpression raw)
	{
		Expression result = null;
		
		Token token = raw.GetNextToken();
		if (token!=null)
		{
			switch (token.Type())
			{
				case Constant:
					result = new Constant(token.Value());
					break;
				case Symbol:
					result = new Symbol(token.Value());
					break;
				case Subexpression:
					result = Parse(token.Value());
					break;
			}
		}
		return result;		
	}
	
	private Token ParseOperator(RawExpression raw)
	{
		Token result = raw.GetNextToken();
		if (result!=null && !result.IsAnOperator())
			result = null;
		
		return result;
	}
}
