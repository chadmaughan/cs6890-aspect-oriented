package AppLayer;

public class Subtract extends BinaryExpression {

	public Subtract(Expression left, Expression right)
	{
		super(left, right);
	}

	@Override
	public Double Evaluate()
	{
		return Left().Evaluate() - Right().Evaluate();
	}

	@Override
	public String toString()
	{
		return "(" + Left().toString() + " - " + Right().toString() + ")";
	}

}
