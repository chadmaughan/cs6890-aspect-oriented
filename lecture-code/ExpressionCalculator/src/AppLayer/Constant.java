package AppLayer;

public class Constant extends Expression
{
	private Double myValue;
	
	public Constant()
	{
		myValue = 0.0;
	}
	
	public Constant(Double origValue)
	{
		myValue = origValue;
	}

	public Constant(double origValue)
	{
		myValue = new Double(origValue);
	}
	
	public Constant(String origValue)
	{
		myValue = new Double(origValue);
	}

	@Override
	public Double Evaluate() {
		return myValue;
	}
	
	@Override
	public String toString()
	{
		return myValue.toString();
	}

}
