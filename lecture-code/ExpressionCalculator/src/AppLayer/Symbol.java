package AppLayer;

public class Symbol extends Expression
{
	private String mySymbol = "Unnamed";
	private Double myValue = 0.0;
	
	public Symbol(String symbol)
	{
		mySymbol = symbol;
	}
	
	public Symbol(String symbol, Double origValue)
	{
		mySymbol = symbol;
		myValue = origValue;
	}
	
	@Override
	public Double Evaluate()
	{
		return myValue;
	}
	
	@Override
	public String toString()
	{
		return mySymbol;
	}

}
