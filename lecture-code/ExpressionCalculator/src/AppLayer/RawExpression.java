package AppLayer;

public class RawExpression
{
	private static String firstSymbolChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static String otherSymbolChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	private static String firstNumberChars = "01234567890.";
	private static String otherNumberChars = "0123456789.";
	private static String whiteSpace = " \t\n";
	
	private String strExpression = null;
	private int currentPosition=0;
	
	public RawExpression(String origStrExpression)
	{
		strExpression = origStrExpression;
	}
	
	public Boolean MoreToParse()
	{
		return (currentPosition<strExpression.length());
	}
	
	public Token GetNextToken()
	{
		Token result = null;

		if (strExpression!=null)
		{
			Boolean startedSymbol = false;
			Boolean startedConstant = false;
			Boolean keepLooking = true;
			int endPos = currentPosition;
			
			while (keepLooking && endPos<strExpression.length())
			{
				char nextChar = strExpression.charAt(endPos);
				if (startedSymbol==false && firstSymbolChars.indexOf(nextChar) >= 0)
					startedSymbol = true;
				else if (startedSymbol==true)
				{
					 if (otherSymbolChars.indexOf(nextChar) < 0)
					 {
						 keepLooking = false;
						 result = new Token(Token.Type.Symbol, strExpression.substring(currentPosition, endPos));
					 }
				}
				else if (startedConstant==false && firstNumberChars.indexOf(nextChar) >= 0)
				{
					startedConstant = true;
				}
				else if (startedConstant==true)
				{
					if (otherNumberChars.indexOf(nextChar) < 0)
					{
						keepLooking = false;
						result = new Token(Token.Type.Constant, strExpression.substring(currentPosition, endPos));
					}
				}				
				else if (nextChar=='(')
				{
					keepLooking = false;
					endPos = FindMatchingClosingParan(endPos+1);
					result = new Token(Token.Type.Subexpression, strExpression.substring(currentPosition+1, endPos));
					endPos++;
				}
				else if (nextChar=='+')
				{
					keepLooking = false;
					result = new Token(Token.Type.Add, "+");
					endPos++;
				}
				else if (nextChar=='-')
				{
					keepLooking = false;
					result = new Token(Token.Type.Subtract, "-");					
					endPos++;
				}
				else if (nextChar=='*')
				{
					keepLooking = false;
					result = new Token(Token.Type.Multiple, "*");
					endPos++;
				}
				else if (nextChar=='/')
				{
					keepLooking = false;
					result = new Token(Token.Type.Divide, "/");
					endPos++;
				}
				else if (whiteSpace.indexOf(nextChar)<0)
				{
					keepLooking = false;
					result = new Token(Token.Type.Illegal, "");
				}
				
				if (keepLooking)
					endPos++;
			}
			if (keepLooking)
			{
				if (startedSymbol)
					result = new Token(Token.Type.Symbol, strExpression.substring(currentPosition));
				else if (startedConstant)
					result = new Token(Token.Type.Constant, strExpression.substring(currentPosition));
			}
			currentPosition = endPos;
		}
		return result;
	}
	
	private int FindMatchingClosingParan(int endPos)
	{
		int found = -1;
		int paranCount = 1;
		while (found==-1 && endPos<strExpression.length())
		{
			char nextChar = strExpression.charAt(endPos);
			if (nextChar=='(')
				paranCount++;
			else if (nextChar==')')
			{
				paranCount--;
				if (paranCount==0)
					found = endPos;
			}
			endPos++;
		}
		
		return found;
	}
}
